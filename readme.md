<p align="center"><img src="storage/images/PCLOGO.png" style="max-width:100%; height:auto;"></p>

## About Project Cargo

Project Cargo is a web application that streamlines supply chain operations, simplify the shipping process and increase logistic efficiency in freight services. This application was created as a project for the ITP module at SLIIT. 

## The Team

- Pinnawala N. M. 
- Bandara R. A. D. M.
- Theeraj S. M. D.
- Lakshan Wijewardana W. M. W.
- Senevirathna B. W. H. K.

## License

Not available. 
